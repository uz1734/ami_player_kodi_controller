# AmI_player_KODI_controller

Projekt, ki s pomočjo Node-Red okolja, TrackMater vrtilnih kod in nekaj JS logike, kontrolira multimedijski predvajalnik KODI

## Namestitev

 * Namestitev Node-red razvojnega okolja 
 * V Node-red razvojno okolje uvozite JSON datoeteko ```KODISeminar.json```  
 * Na bloku ```/TRACKMATE/DATA``` določite ```IP in PORT``` vašega ```mqtt-broker-ja```  
 * V edini funkciji, ki ni poimenovana spremenite oz. določite ```ID``` vrtilnih tagov  
 * Zaženite Vas KODI predvajalnik, ki si ga lahko naložite s spletne strani:  https://kodi.tv/  
 * V aplikaciji KODI pod ```Settings/Services/Control``` omogočite oddaljeno upravljanje predvajalnika. Ostale nastavitve pustite privzete  
 * Zaženite Node-Red aplikacijo

### Konfiguracija trackmate

Za upravljanje predvajalnika boste potrebovali konfigurirati program za detekcijo vrtilnih značk imenovan ```trackmate```. Izberite si 3 vrtilne značke in definirajte
območje zaznavanja kamere in ```trackmate```. Določite ordinatno in abcisno os programa ```trackmate```, saj sta pomembni pri zaznavanju klika v predvajalniku

## Uporabniško upravljanje predvajalnika

### Zečetno stanje (Zaznavamo 0 tagov)

Če ne zaznavamo nobenega taga, se multimedijski predvajalnik KODI vrne na osnovno začetno stanje in čaka na nadalnje ukaze.

### Stanje kontrole KODI (zazanvamo 2 taga)

Če sta zaznavamo 2 taga, kontroliramo vmenisk in sprehajanje po menijih in funkcijah predvajalnika KODI

 * Z rotacijo prvega taga, kontrolirate premikanje levo/desno po predvajalniku  
 * Z rotacijo drugega vrtilnega taga, kontrolirate premikanje gor/dol po predvajlniku  
 * S približevanjem in nato oddaljevanjem obeh tagov na abcisni osi sprožite klik in premik v izbrano kategorijo  
 * S približevanjem in nato oddaljevanjem obeh tagov na ordinatni osi sprožite klik in premik nazaj iz izbrane kategorije 

### Stanje kontrole predvajalnika slik (zaznavamo 3 tage)

Če položimo še en tag in jih imamo in zaznavamo 3, bomo prvima dvema zamenjali način delovanja. Z enakimi gibi kot so opisani v prejšnjem poglavju, sedaj kontroliramo
predvajalnik slik in sicer:

 * Z rotacijo prvega ali drugega taga, se manualno premaknite na naslednjo/prejšnjo sliko 
 * S približevanjem in nato oddaljevanjem obeh tagov na abcisni osi sprožite klik in zagon/zaustavitev predvajalnika, ki avtomatsko menja slike  
 * S približevanjem in nato oddaljevanjem obeh tagov na ordinatni osi sprožite klik in izhod iz predvajalnika

